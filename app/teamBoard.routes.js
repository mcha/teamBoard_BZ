(function(){
    angular.module('teamBoard.app')
        .config(routes);

    function routes($stateProvider) {
        var teamState = {
            name: 'test1',
            url: 'app/team/team',
            template: '<h1>Team Rocket routes State</h1>'
        }

        var boardState = {
            name: 'board1',
            url: "app/board/board",
            template: '<h3>Its the UI-Router Team Body app!</h3>'
        }
        $stateProvider.state(teamState);
        $stateProvider.state(boardState);
    }
})();

